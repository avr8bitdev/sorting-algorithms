/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Sorting_functions.h"


// --- internal --- //
static void Sort_vidSelection_asc(s32* vidPtrArrCpy, const u16 u16LenCpy)
{
    for (u16 current_idx = 0; current_idx < u16LenCpy; current_idx++) // foreach element is src
    {
        u16 idx_of_least = current_idx; // assume smallest is current element
        for (u16 next_idx = current_idx + 1; next_idx < u16LenCpy; next_idx++) // starting from next element after current one
        {
            if (vidPtrArrCpy[next_idx] < vidPtrArrCpy[idx_of_least])
                idx_of_least = next_idx;
        }

        if (idx_of_least != current_idx) // if a smaller no. is found, then swap
        {
            s32 tmp_ = vidPtrArrCpy[current_idx];
            vidPtrArrCpy[current_idx] = vidPtrArrCpy[idx_of_least];
            vidPtrArrCpy[idx_of_least] = tmp_;
        }
    }
}

static void Sort_vidSelection_desc(s32* vidPtrArrCpy, const u16 u16LenCpy)
{
    for (u16 current_idx = 0; current_idx < u16LenCpy; current_idx++) // foreach element is src
    {
        u16 idx_of_max = current_idx; // assume largest is current element
        for (u16 next_idx = current_idx + 1; next_idx < u16LenCpy; next_idx++) // starting from next element after current one
        {
            if (vidPtrArrCpy[next_idx] > vidPtrArrCpy[idx_of_max])
                idx_of_max = next_idx;
        }

        if (idx_of_max != current_idx) // if a larger no. is found, then swap
        {
            s32 tmp_ = vidPtrArrCpy[current_idx];
            vidPtrArrCpy[current_idx] = vidPtrArrCpy[idx_of_max];
            vidPtrArrCpy[idx_of_max] = tmp_;
        }
    }
}
// ---------------- //

void Sort_vidSelection(s32* vidPtrArrCpy, const u16 u16LenCpy, Sort_dir_t enumSortDirCpy)
{
    switch (enumSortDirCpy)
    {
        case Sort_asc:
            Sort_vidSelection_asc(vidPtrArrCpy, u16LenCpy);
        break;

        case Sort_desc:
            Sort_vidSelection_desc(vidPtrArrCpy, u16LenCpy);
        break;
    }
}
